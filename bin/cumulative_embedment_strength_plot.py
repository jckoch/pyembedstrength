#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 11:03:41 2020

@author: jckoch
"""

import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pyembedstrength.core import cdf_embedment_strength


def main(fname):
    # load experimental dataset
    df_in = pd.read_csv(fname, na_values="")
    N = 10000  # number of samples to simulate

    # Figure 1: Original dataset from Leijten et al. (2004)
    df = df_in[df_in['Source'] != 'Kennedy et al. (2014)']
    wood_types = df['Wood Type'].unique()
    fastener_types = df['Fastener Type'].unique()

    linestyles = [(0, (1, 7)),
                  (0, (1, 5)),
                  (0, (1, 3)),
                  (0, (1, 1)),
                  (0, (3, 7, 1, 7)),
                  (0, (3, 5, 1, 5)),
                  (0, (3, 3, 1, 3)),
                  (0, (3, 1, 1, 1)),  
                  (0, (5, 7)),
                  (0, (5, 5)),
                  (0, (5, 3)),
                  (0, (5, 1))]
    
    labels = ['Experimental $\\parallel$-to-grain',
              'EC5 $\\parallel$-to-grain',
              'O86 $\\parallel$-to-grain',
              'Leijten model $\\parallel$-to-grain',
              'Kennedy model $\\parallel$-to-grain',
              'Experimental $\\perp$-to-grain',
              'EC5 $\\perp$-to-grain',
              'O86 $\\perp$-to-grain',
              'Leijten model $\\perp$-to-grain',
              'Kennedy model $\\perp$-to-grain']

    markerstyles1 = ['x', 'o']
    markerstyles2 = ['x', '+', 'o']
    
    h1 = 0
    h2 = 0
    
    load_angles = df['Loading Angle (deg)'].unique()
    m = len(wood_types) * len(fastener_types) * len(load_angles)
    fh_exp = np.zeros((N, m*2))
    fh = np.zeros((N, m*4))

    # fig, axes = plt.subplots(nrows=len(wood_types), ncols=len(fastener_types),
    #                           sharey=True, figsize=(9.2, 7.4))
    for idw, w in enumerate(wood_types):
        for idf, f in enumerate(fastener_types):            
            fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(9.2, 7.4))
            
            data = df[df['Wood Type'] == w]
            data = data[data['Fastener Type'] == f]
            x = np.linspace(0, 1, N)
            f1 = 0
            m1 = 0

            for idx, la in enumerate(load_angles):
                data_ang = data[data['Loading Angle (deg)'] == la]
            
                if not data.empty:
                    exp, pred, R2 = cdf_embedment_strength(data_ang, N, la,
                                                           wood_type=w,
                                                           fastener_type=f)
                fh_exp[0:len(exp), h1:h1+2] = exp
                fh[:, h2:h2+4] = pred

                # plotting [idw, idf]
                axes.set_title(('wood: %s & fastener: %s\n' +
                                'Explained variance in regression' +
                                ' model: %3.0f%% (%2.0f%%)') % (
                                    w, f, R2[0]*100, R2[1]*100))
                
                axes.plot(fh_exp[0:len(exp):8, h1+1], 
                          fh_exp[0:len(exp):8, h1],
                          color='k', marker=markerstyles1[m1],
                          alpha=0.1)
                axes.plot(fh[:, h2], x, 'k', linestyle=linestyles[f1])
                axes.plot(fh[:, h2+1], x, 'k', linestyle=linestyles[f1+1])
                axes.plot(fh[:, h2+2], x, 'k', linestyle=linestyles[f1+2])
                axes.plot(fh[:, h2+3], x, 'k', linestyle=linestyles[f1+3])

                h1 += 2
                h2 += 4
                f1 += 4
                m1 += 1               

            # axes.set_xlabel('Cumulative distribution function')
            # axes.set_ylabel('Embedment strength (MPa)')
            
            fig.legend(labels, loc='upper center', ncol=2)
            fig.tight_layout(rect=(0, 0, 1, 0.80))
            fname = '../../../thesis/figure/fh_comparison_%s_%s.pdf' % (w, f)
            fig.savefig(fname)

    # Figure 2: Newer dataset from Kennedy et al. (2014)
    # To illustrate the difference between test methods. Should match closer
    # with O86 predictions.
    kennedy = df_in[df_in['Source'] == 'Kennedy et al. (2014)']
    kennedy = kennedy.dropna()
    wood_types = kennedy['Wood Type'].unique()
    fastener_types = kennedy['Fastener Type'].unique()

    h1 = 0
    h2 = 0
    
    load_angles = kennedy['Loading Angle (deg)'].unique()
    m = len(wood_types) * len(fastener_types) * len(load_angles)
    fh_exp = np.zeros((N, m*2))
    fh = np.zeros((N, m*4))
    

    fig2, ax = plt.subplots(nrows=1, ncols=1, figsize=(9.2, 7.4))
    for idw, w in enumerate(wood_types):
        for idf, f in enumerate(fastener_types):
            data = kennedy[kennedy['Wood Type'] == w]
            data = data[data['Fastener Type'] == f]
            x = np.linspace(0, 1, N)
            f1 = 0    
            m1 = 0

            for idx, la in enumerate(load_angles):
                data_ang = data[data['Loading Angle (deg)'] == la]
            
                if not data.empty:
                    exp, pred, R2 = cdf_embedment_strength(data_ang, N, la,
                                                           wood_type=w,
                                                           fastener_type=f)
                fh_exp[0:len(exp), h1:h1+2] = exp
                fh[:, h2:h2+4] = pred
        
                # plotting
                ax.plot(fh_exp[0:len(exp):4, h1+1], fh_exp[0:len(exp):4, h1],
                        color='k', marker=markerstyles2[m1], alpha=0.1)
                ax.plot(fh[:, h2], x, 'k', linestyle=linestyles[f1])
                ax.plot(fh[:, h2+1], x, 'k', linestyle=linestyles[f1+1])
                ax.plot(fh[:, h2+2], x, 'k', linestyle=linestyles[f1+2])
                ax.plot(fh[:, h2+3], x, 'k', linestyle=linestyles[f1+3])
                
                ax.set_title(('wood: %s & fastener: %s\n' +
                                          'Explained variance in regression' +
                                          ' model: %3.0f%% (%2.0f%%)') % (
                                              w, f, R2[0]*100, R2[1]*100))
                
                h1 += 2
                h2 += 4
                f1 += 4
                m1 += 1

    ax.set_xlabel('Embedment Strength (MPa)')
    ax.set_ylabel('Cumulative density function')
    labels = ['Experimental $\\parallel$-to-grain',
              'EC5 $\\parallel$-to-grain',
              'O86 $\\parallel$-to-grain',
              'Leijten model $\\parallel$-to-grain',
              'Kennedy model $\\parallel$-to-grain',
              'Experimental $\\perp$-to-grain',
              'EC5 $\\perp$-to-grain',
              'O86 $\\perp$-to-grain',
              'Leijten model $\\perp$-to-grain',
              'Kennedy model $\\perp$-to-grain',
              'Experimental 45 degree-to-grain',
              'EC5 45 degree-to-grain',
              'O86 45 degree-to-grain',
              'Leijten model 45 degree-to-grain',
              'Kennedy model 45 degree-to-grain']
    fig2.legend(labels, loc='upper center', ncol=3)
    fig2.tight_layout(rect=(0, 0, 1, 0.825))
    fig2.savefig('../../../thesis/figure/embedmentstrength_evalmethod2.pdf')


if __name__ == '__main__':
    try:
        fname = sys.argv[1]
    except IndexError:
        fname = ('../../../embedment-study/data/'
                 'embedmentstrengthdata_multiplesourcedatabase.csv')

    main(fname)
