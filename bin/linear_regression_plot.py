#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 10 20:15:59 2020

@author: jkoch
"""

# linfit.py - example of confidence limit calculation for linear regression fitting.
 
# References:
# - Statistics in Geography by David Ebdon (ISBN: 978-0631136880)
# - Reliability Engineering Resource Website:
# - http://www.weibull.com/DOEWeb/confidence_intervals_in_simple_linear_regression.htm
# - University of Glascow, Department of Statistics:
# - http://www.stats.gla.ac.uk/steps/glossary/confidence_intervals.html#conflim
 
import pandas as pd
import matplotlib.pyplot as plt
from pyembedstrength.linfit import linfit


# load experimental dataset
fname = '../../../embedment-study/data/embedmentstrengthdata_multiplesourcedatabase.csv'
data = pd.read_csv(fname, na_values="")

# filter extraneous dataset
data = data[data['Source'] != 'Kennedy et al. (2014)']
cf = data[data['Wood Type'] == 'coniferous']
cf_parallel = cf[cf['Loading Angle (deg)'] == 0]
cf_perp = cf[cf['Loading Angle (deg)'] == 90]
dc = data[data['Wood Type'] == 'deciduous']
dc_parallel = dc[dc['Loading Angle (deg)'] == 0]
dc_perp = dc[dc['Loading Angle (deg)'] == 90]

# Plot (0, 0) == 1
y_1a = cf_parallel['Embedment Strength (MPa)'].to_numpy()
x_1a = cf_parallel['Density (kg/m3)'].to_numpy()
p_x_1a, c_x_1a, c_y_1a, lower_1a, upper_1a = linfit(x_1a, y_1a)
y_1b = dc_parallel['Embedment Strength (MPa)'].to_numpy()
x_1b = dc_parallel['Density (kg/m3)'].to_numpy()
p_x_1b, c_x_1b, c_y_1b, lower_1b, upper_1b = linfit(x_1b, y_1b)

# Plot (0, 1) == 2
y_2a = cf_parallel['Embedment Strength (MPa)'].to_numpy()
x_2a = cf_parallel['Diameter (mm)'].to_numpy()
p_x_2a, c_x_2a, c_y_2a, lower_2a, upper_2a = linfit(x_2a, y_2a)
y_2b = dc_parallel['Embedment Strength (MPa)'].to_numpy()
x_2b = dc_parallel['Diameter (mm)'].to_numpy()
p_x_2b, c_x_2b, c_y_2b, lower_2b, upper_2b = linfit(x_2b, y_2b)

# Plot (1, 0) == 3
y_3a = cf_perp['Embedment Strength (MPa)'].to_numpy()
x_3a = cf_perp['Density (kg/m3)'].to_numpy()
p_x_3a, c_x_3a, c_y_3a, lower_3a, upper_3a = linfit(x_3a, y_3a)
y_3b = dc_perp['Embedment Strength (MPa)'].to_numpy()
x_3b = dc_perp['Density (kg/m3)'].to_numpy()
p_x_3b, c_x_3b, c_y_3b, lower_3b, upper_3b = linfit(x_3b, y_3b)

# Plot (1, 1) == 4
y_4a = cf_perp['Embedment Strength (MPa)'].to_numpy()
x_4a = cf_perp['Diameter (mm)'].to_numpy()
p_x_4a, c_x_4a, c_y_4a, lower_4a, upper_4a = linfit(x_4a, y_4a)
y_4b = dc_perp['Embedment Strength (MPa)'].to_numpy()
x_4b = dc_perp['Diameter (mm)'].to_numpy()
p_x_4b, c_x_4b, c_y_4b, lower_4b, upper_4b = linfit(x_4b, y_4b)

# set-up the plot
fig, axes = plt.subplots(nrows=2, ncols=2, sharey=True,
                         figsize=(9.2, 7.4))

# plot line of best fit
axes[0, 0].plot(c_x_1a, c_y_1a, 'k-')
axes[0, 0].plot(c_x_1b, c_y_1b, 'k--')
axes[1, 0].plot(c_x_2a, c_y_2a, 'k-')
axes[1, 0].plot(c_x_2b, c_y_2b[::-1], 'k--')

axes[0, 1].plot(c_x_3a, c_y_3a, 'k-')
axes[0, 1].plot(c_x_3b, c_y_3b, 'k--')
axes[1, 1].plot(c_x_4a, c_y_4a[::-1], 'k-')
axes[1, 1].plot(c_x_4b, c_y_4b[::-1], 'k--')
 
# plot confidence limits
axes[0, 0].plot(p_x_1a, lower_1a, 'k:')
axes[0, 0].plot(p_x_1a, upper_1a, 'k:')
axes[0, 0].plot(p_x_1b, lower_1b, 'k:')
axes[0, 0].plot(p_x_1b, upper_1b, 'k:')
axes[1, 0].plot(p_x_2a, lower_2a, 'k:')
axes[1, 0].plot(p_x_2a, upper_2a, 'k:')
axes[1, 0].plot(p_x_2b, lower_2b, 'k:')
axes[1, 0].plot(p_x_2b, upper_2b, 'k:')

axes[0, 1].plot(p_x_3a, lower_3a, 'k:')
axes[0, 1].plot(p_x_3a, upper_3a, 'k:')
axes[0, 1].plot(p_x_3b, lower_3b, 'k:')
axes[0, 1].plot(p_x_3b, upper_3b, 'k:')
axes[1, 1].plot(p_x_4a, lower_4a, 'k:')
axes[1, 1].plot(p_x_4a, upper_4a, 'k:')
axes[1, 1].plot(p_x_4b, lower_4b, 'k:')
axes[1, 1].plot(p_x_4b, upper_4b, 'k:')

# plot axes labels 
axes[0, 0].set_xlabel('Density ($kg/m^3$)')
axes[0, 1].set_xlabel('Density ($kg/m^3$)')
axes[1, 0].set_xlabel('Diameter (mm)')
axes[1, 1].set_xlabel('Diameter (mm)')
axes[0, 0].set_ylabel('Embedment Strength (MPa)')
axes[1, 0].set_ylabel('Embedment Strength (MPa)')

# column axes titles
axes[0, 0].set_title('$\parallel$-to-grain')
axes[0, 1].set_title('$\perp$-to-grain')

# configure legend
labels = ['Coniferous', 'Deciduous']
axes[0, 0].legend(labels, loc='best')
axes[0, 1].legend(labels, loc='best')
axes[1, 0].legend(labels, loc='best')
axes[1, 1].legend(labels, loc='best')

plt.tight_layout(h_pad=2.08)
fig.savefig('../../../thesis/figure/embedmentstrength_dowelregplot.pdf')
fig.savefig('../../../thesis_presentation/figure/embedmentstrength_dowelregplot.pdf')
