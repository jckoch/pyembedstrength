#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 11:03:41 2020

@author: jckoch
"""

import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})
from pyembedstrength.core import cdf_embedment_strength
from pyembedstrength.presentation_plot import nstep_plot


def main(fname):
    # load experimental dataset
    df_in = pd.read_csv(fname, na_values="")
    N = 10000  # number of samples to simulate

    # Figure 1: Original dataset from Leijten et al. (2004)
    df = df_in[df_in['Source'] != 'Kennedy et al. (2014)']
    wood_types = df['Wood Type'].unique()
    fastener_types = df['Fastener Type'].unique()

    labels = ['Experimental $\\parallel$-to-grain',
              'EC5 $\\parallel$-to-grain',
              'O86 $\\parallel$-to-grain',
              'Leijten model $\\parallel$-to-grain',
              'Kennedy model $\\parallel$-to-grain',
              'Experimental $\\perp$-to-grain',
              'EC5 $\\perp$-to-grain',
              'O86 $\\perp$-to-grain',
              'Leijten model $\\perp$-to-grain',
              'Kennedy model $\\perp$-to-grain']
    
    load_angles = df['Loading Angle (deg)'].unique()
    m = len(wood_types) * len(fastener_types) * len(load_angles)
    
    for idw, w in enumerate(wood_types):
        for idf, f in enumerate(fastener_types):            
            num = 0
            
            fh_exp = np.zeros((N, m*2))
            fh = np.zeros((N, m))
        
            h1 = 0
            h2 = 0
                        
            data = df[df['Wood Type'] == w]
            data = data[data['Fastener Type'] == f]
            x = np.linspace(0, 1, N)

            f1 = 0
            m1 = 0
            l1 = 0

            # plotting
            fig1, axes = plt.subplots(nrows=1, ncols=1, figsize=(9.2, 7.4))

            for idx, la in enumerate(load_angles):
                data_ang = data[data['Loading Angle (deg)'] == la]
            
                if not data.empty:
                    exp, pred, R2 = cdf_embedment_strength(data_ang, N, la,
                                                           wood_type=w,
                                                           fastener_type=f)
                fh_exp[0:len(exp), h1:h1+2] = exp
                fh[:, h2:h2+4] = pred

                titlestr = ('wood: %s & fastener: %s\n' +
                            'Explained variance in regression' +
                            ' model: %3.0f%% (%2.0f%%)') % (w, f,
                                                            R2[0]*100,
                                                            R2[1]*100)

                axes.set_title(titlestr, size=16)
                
                index = (h1, h2, f1, m1)
                figval = (w, f, num)
                axes, num = nstep_plot(fig1, axes, exp, x, pred, index, figval,
                                       labels[l1:l1+5])

                h1 += 2
                h2 += 4
                f1 += 4
                m1 += 1
                l1 += 5

            axes.set_xlabel('Cumulative distribution function')
            axes.set_ylabel('Embedment strength (MPa)')
            
            axes.legend(loc='best', fontsize=12)
            fig1.tight_layout() #rect=(0, 0, 1, 0.80)
            fname = '../results/fh_comparison_%s_%s.pdf' % (w, f)
            #fname = '../../../thesis/figure/fh_comparison_%s_%s.pdf' % (w, f)
            fig1.savefig(fname)

    # Figure 2: Newer dataset from Kennedy et al. (2014)
    # To illustrate the difference between test methods. Should match closer
    # with O86 predictions.
    kennedy = df_in[df_in['Source'] == 'Kennedy et al. (2014)']
    kennedy = kennedy.dropna()
    wood_types = kennedy['Wood Type'].unique()
    fastener_types = kennedy['Fastener Type'].unique()
    
    load_angles = kennedy['Loading Angle (deg)'].unique()
    m = len(wood_types) * len(fastener_types) * len(load_angles)
    
    markerstyles = ['x', '+', 'o']
    
    labels = ['Experimental $\\parallel$-to-grain',
          'EC5 $\\parallel$-to-grain',
          'O86 $\\parallel$-to-grain',
          'Leijten model $\\parallel$-to-grain',
          'Kennedy model $\\parallel$-to-grain',
          'Experimental $\\perp$-to-grain',
          'EC5 $\\perp$-to-grain',
          'O86 $\\perp$-to-grain',
          'Leijten model $\\perp$-to-grain',
          'Kennedy model $\\perp$-to-grain',
          'Experimental 45 degree-to-grain',
          'EC5 45 degree-to-grain',
          'O86 45 degree-to-grain',
          'Leijten model 45 degree-to-grain',
          'Kennedy model 45 degree-to-grain']

    fig2, ax = plt.subplots(nrows=1, ncols=1, figsize=(9.2, 7.4))
    
    for idw, w in enumerate(wood_types):
        for idf, f in enumerate(fastener_types):
            num = 0
            
            fh_exp = np.zeros((N, m*2))
            fh = np.zeros((N, m*4))
            
            h1 = 0
            h2 = 0
            
            data = kennedy[kennedy['Wood Type'] == w]
            data = data[data['Fastener Type'] == f]
            x = np.linspace(0, 1, N)
            f1 = 0    
            m1 = 0
            l1 = 0

            for idx, la in enumerate(load_angles):
                data_ang = data[data['Loading Angle (deg)'] == la]
            
                if not data.empty:
                    exp, pred, R2 = cdf_embedment_strength(data_ang, N, la,
                                                           wood_type=w,
                                                           fastener_type=f)
                fh_exp[0:len(exp), h1:h1+2] = exp
                fh[:, h2:h2+4] = pred

                # plotting                
                titlestr = ('wood: %s & fastener: %s\n' +
                            'Explained variance in regression' +
                            ' model: %3.0f%% (%2.0f%%)') % (w, f,
                                                            R2[0]*100,
                                                            R2[1]*100)
                ax.set_title(titlestr)
                
                index = (h1, h2, f1, m1)
                figval = (w, f, num)
                ax, num = nstep_plot(fig2, ax, exp, x, pred, index, figval,
                                     labels[l1:l1+5], markerstyle=markerstyles,
                                     step=4)
                
                h1 += 2
                h2 += 4
                f1 += 4
                m1 += 1
                l1 += 5

    ax.set_xlabel('Embedment Strength (MPa)')
    ax.set_ylabel('Cumulative density function')

    ax.legend(labels, loc='lower right', ncol=1, fontsize=12)
    fig2.tight_layout() #rect=(0, 0, 1, 0.825)
    fig2.savefig('../results/embedmentstrength_evalmethod2.pdf')
    # fig2.savefig('../../../thesis/figure/embedmentstrength_evalmethod2.pdf')


if __name__ == '__main__':
    try:
        fname = sys.argv[1]
    except IndexError:
        fname = ('../../../embedment-study/data/'
                 'embedmentstrengthdata_multiplesourcedatabase.csv')

    main(fname)
