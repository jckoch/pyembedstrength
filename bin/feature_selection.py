# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LassoCV

fname = '../../../data/embedmentstrengthdata_multiplesourcedatabase.csv'

# load experimental dataset
original_data = pd.read_csv(fname, na_values="")

# filter dataset to consider European data
data = original_data[original_data['Source'] != 'Kennedy et al. (2014)']
data.dropna()

X = data
X = data.drop(columns=['Embedment Strength (MPa)', 'Source', 'Threaded?', 'Evaluation Method'])

map_ftype = {'nails': 0, 'dowels':1}
X['Fastener Type'] = X['Fastener Type'].map(map_ftype)

unique_species = X['Wood Species'].unique()
unique_num = np.arange(0, len(unique_species))
unique_num = np.arange(0, len(unique_species))
map_species = dict(zip(unique_species, unique_num))
X['Wood Species'] = X['Wood Species'].map(map_species)

map_wtype = {'coniferous': 0, 'deciduous': 1}
X['Wood Type'] = X['Wood Type'].map(map_wtype)

y = data['Embedment Strength (MPa)']

reg = LassoCV()
reg.fit(X, y)

print("Best alpha using built-in LassoCV: %f" % reg.alpha_)
print("Best score using built-in LassoCV: %f" %reg.score(X,y))

coef = pd.Series(reg.coef_, index = X.columns)
print("Lasso picked " + str(sum(coef != 0)) + " variables and eliminated the other " +  str(sum(coef == 0)) + " variables")

imp_coef = coef.sort_values()

imp_coef.plot(kind = "barh")
plt.title("Feature importance using Lasso Model")