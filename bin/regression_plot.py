#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 11:03:41 2020

@author: jckoch
"""

import sys
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from pyembedstrength.seabornFig2Grid import SeabornFig2Grid


def main(fname):
    # load experimental dataset
    data = pd.read_csv(fname, na_values="")

    # filter extraneous dataset
    data1 = data[data['Source'] != 'Kennedy et al. (2014)']
    data1 = data1[data1['Diameter (mm)'] != 5]
    data1 = data1[data1['Diameter (mm)'] != 7]

    # fig1 = plt.figure(figsize=(9.2, 7.4))
    f1 = sns.lmplot(x='Diameter (mm)', y='Embedment Strength (MPa)',
                       hue='Wood Type', scatter=False, data=data1)

    # fig2 = plt.figure(figsize=(9.2, 7.4))
    f2 = sns.lmplot(x='Density (kg/m3)', y='Embedment Strength (MPa)',
                       hue='Wood Type', scatter=False, data=data1)

    fig1 = plt.figure(figsize=(9.2, 7.4))
    gs = gridspec.GridSpec(1, 2)
    SeabornFig2Grid(f1, fig1, gs[0])
    SeabornFig2Grid(f2, fig1, gs[1])

    # group dataset to cases interested in
    parallel = data1[data1['Loading Angle (deg)'] == 0]
    parallel_cf = parallel[parallel['Wood Type'] == 'coniferous']
    parallel_dd = parallel[parallel['Wood Type'] == 'deciduous']

    perpendicular = data1[data1['Loading Angle (deg)'] == 90]
    perpendicular_cf = perpendicular[perpendicular['Wood Type'] == 'coniferous']
    perpendicular_dd = perpendicular[perpendicular['Wood Type'] == 'deciduous']

    # linear regression plots using seaborn
    s0 = sns.lmplot(x='Density (kg/m3)', y='Embedment Strength (MPa)',
                    hue='Diameter (mm)', scatter=False, data=parallel_cf)
    s1 = sns.lmplot(x='Density (kg/m3)', y='Embedment Strength (MPa)',
                    hue='Diameter (mm)', scatter=False, data=perpendicular_cf)
    s2 = sns.lmplot(x='Density (kg/m3)', y='Embedment Strength (MPa)',
                    hue='Diameter (mm)', scatter=False, data=parallel_dd)
    s3 = sns.lmplot(x='Density (kg/m3)', y='Embedment Strength (MPa)',
                    hue='Diameter (mm)', scatter=False, data=perpendicular_dd)

    fig3 = plt.figure(figsize=(9.2, 7.4))
    gs = gridspec.GridSpec(2, 2)
    SeabornFig2Grid(s0, fig3, gs[0])
    SeabornFig2Grid(s2, fig3, gs[1])
    SeabornFig2Grid(s1, fig3, gs[2])
    SeabornFig2Grid(s3, fig3, gs[3])


if __name__ == '__main__':
    try:
        fname = sys.argv[1]
    except IndexError:
        fname = '../../../embedment-study/data/embedmentstrengthdata_multiplesourcedatabase.csv'

    main(fname)
