try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def readme():
    with open('README.org') as f:
        return f.read()


setup(name='pytimber',
      version='0.1.0',
      description='',
      long_description=readme(),
      classifiers=[],
      keywords='',
      url='',
      author='jchkoch',
      author_email='jchkoch@gmail.com',
      license='GPLv2',
      packages=[],
      install_requires=['numpy', 'matplotlib', 'pandas', 'scipy', 'sklearn'],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      entry_points={},
      scripts=['bin/data_analysis'],
      include_package_data=True,
      zip_safe=False)
