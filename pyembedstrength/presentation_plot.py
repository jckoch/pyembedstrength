# -*- coding: utf-8 -*-


def nstep_plot(fig, axes, exp, x, pred, index, figval, labels,
               linestyle=None, markerstyle=None, step=None):
    """
    Plot multiple data sets in sequence saving each intermediate plot.

    Returns
    -------
    figure.

    """

    if linestyle is None:
        linestyles = [(0, (1, 7)), (0, (1, 5)), (0, (1, 3)), (0, (1, 1)),
                      (0, (3, 7, 1, 7)), (0, (3, 5, 1, 5)), 
                      (0, (3, 3, 1, 3)), (0, (3, 1, 1, 1)),
                      (0, (5, 7)), (0, (5, 5)), (0, (5, 3)), (0, (5, 1))]
    else:
        linestyles = linestyle
            
    if markerstyle is None:
        markerstyles = ['x', 'o']
    else:
        markerstyles = markerstyle
    
    if step is None:
        step = 8
    else:
        step = step
    
    colors = ['b', 'g', 'r', 'c']
    
    num = figval[2]
          
    axes.plot(exp[0:len(exp):step, 1], exp[0:len(exp):step, 0],
              color='k', lw=4, marker=markerstyles[index[3]], alpha=0.1,
              label=labels[0])
    
    for i in range(pred.shape[1]):
        axes.plot(pred[:, i], x, color=colors[i], linestyle=linestyles[index[2]+i],
                  lw=4, label=labels[i+1])
        axes.set_xlabel('Cumulative distribution function')
        axes.set_ylabel('Embedment strength (MPa)')
        axes.legend(loc='lower right', fontsize=12)
        fig.savefig('../results/figure-%s-%s-step-%d.pdf' % (figval[0],
                                                             figval[1], num))
        num += 1
     
    return(axes, num)
