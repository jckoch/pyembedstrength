#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 12:27:03 2020
@author: jckoch
"""

import numpy as np
from scipy.stats import norm, rv_discrete
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import explained_variance_score
from sklearn.metrics import make_scorer, mean_squared_error


def simulate_properties(N, data):
    """
    Simulate properties to predict the embedment strength
    of timber.

    Parameters
    ----------
    data : pandas dataframe
        Input experimental data.
    N : int
        Number of samples to simulate.

    Returns
    -------
    rho : numpy ndarray
        Simulated density samples.
    d : numpy ndarray
        Simulated diameter samples.
    """

    # simulate rho
    mu = data['Density (kg/m3)'].mean()
    sigma = data['Density (kg/m3)'].std()
    rho = norm(loc=mu, scale=sigma).rvs(size=N)

    # simulate d
    xk = data['Diameter (mm)'].unique()
    pk = (1 / len(xk)) * np.ones(len(xk))
    d = rv_discrete(values=(xk, pk)).rvs(size=N)

    return(rho, d)


def embedment_strength_dowels(rho, d, la, reg_leijten):
    """
    Embedment strength of dowelled connections.

    Parameters
    ----------
    rho : numpy ndarray
        Simulated density samples.
    d : numpy ndarray
        Simulated diameter samples.
    la : int
        Loading angle.
    reg_leijten : object
        Linear regression parameters.

    Returns
    -------
    fh : numpy ndarray
        Computed embedment strength.
    """

    X = np.vstack([np.log(rho), np.log(d)]).T
    fh = np.zeros((len(rho), 4))
    if la == 0:
        fh[:, 0] = np.sort(0.082 * rho * (1 - 0.01 * d))
        fh[:, 1] = fh[:, 0]
        fh[:, 2] = np.sort(np.exp(reg_leijten.predict(X)))
        fh[:, 3] = np.sort(108 * (rho/1000)**1.7)
    elif la == 90:
        fh[:, 0] = np.sort((0.082 * rho * (1 - 0.01 * d)) / (1.35 + 0.015 * d))
        fh[:, 1] = np.sort(0.036 * rho * (1 - 0.01 * d))
        fh[:, 2] = np.sort(np.exp(reg_leijten.predict(X)))
        fh[:, 3] = np.sort(70 * (rho/1000)**(2.2))
    else:
        EC5 = ((0.082 * rho * d**(-0.3)) * np.sin(np.deg2rad(la))**2) / (
            (1.35 + 0.015 * d) * np.sin(np.deg2rad(la))**2 +
            np.cos(np.deg2rad(la))**2)
        fh[:, 0] = np.sort(EC5)
        O86 = (0.036 * rho * (1 - 0.01 * d) * np.sin(np.deg2rad(la))**2) / (
            2.27 * np.sin(np.deg2rad(la))**2 + np.cos(np.deg2rad(la))**2)
        fh[:, 1] = np.sort(O86)
        fh[:, 2] = np.sort(np.exp(reg_leijten.predict(X)))
        KNDY = (108 * (rho/1000)**1.7 * np.sin(np.deg2rad(la))**2) / (
            1.54 * (rho/1000)**(-0.5) * np.sin(np.deg2rad(la))**2 +
            np.cos(np.deg2rad(la))**2)
        fh[:, 3] = np.sort(KNDY)

    return(fh)


def embedment_strength_nails(rho, d, la, reg_leijten):
    """
    Embedment strength of nailed connections.

    Parameters
    ----------
    rho : numpy ndarray
        Simulated density samples.
    d : numpy ndarray
        Simulated diameter samples.
    la : int
        Loading angle.
    reg_leijten : object
        Linear regression parameters.

    Returns
    -------
    fh : numpy ndarray
        Computed embedment strength.
    """

    X = np.vstack([np.log(rho), np.log(d)]).T
    fh = np.zeros((len(rho), 4))
    if la == 0:
        fh[:, 0] = np.sort(0.082 * rho * d**(-0.3))
        fh[:, 1] = np.sort((0.082 * rho * (1 - 0.01 * d)) / (1.35 + 0.015 * d))
        fh[:, 2] = np.sort(np.exp(reg_leijten.predict(X)))
        fh[:, 3] = np.sort(108 * (rho/1000)**1.7)
    elif la == 90:
        fh[:, 0] = np.sort((0.082 * rho * d**(-0.3)) / (1.35 + 0.015 * d))
        fh[:, 1] = np.sort(0.036 * rho * (1 - 0.01 * d))
        fh[:, 2] = np.sort(np.exp(reg_leijten.predict(X)))
        fh[:, 3] = np.sort(70 * (rho/1000)**(2.2))
    else:
        EC5 = ((0.082 * rho * d**(-0.3)) * np.sin(np.deg2rad(la))**2) / (
            (1.35 + 0.015 * d) * np.sin(np.deg2rad(la))**2 +
            np.cos(np.deg2rad(la))**2)
        fh[:, 0] = np.sort(EC5)
        O86 = (0.036 * rho * (1 - 0.01 * d) * np.sin(np.deg2rad(la))**2) / (
            2.27 * np.sin(np.deg2rad(la))**2 + np.cos(np.deg2rad(la))**2)
        fh[:, 1] = np.sort(O86)
        fh[:, 2] = np.sort(np.exp(reg_leijten.predict(X)))
        KNDY = (108 * (rho/1000)**1.7 * np.sin(np.deg2rad(la))**2) / (
            1.54 * (rho/1000)**(-0.5) * np.sin(np.deg2rad(la))**2 +
            np.cos(np.deg2rad(la))**2)
        fh[:, 3] = np.sort(KNDY)

    return(fh)


def leijten_model(X_train, y_train):
    """
    Linear regression of experimental data as described
    in Leijten et al. (2004) or Kennedy et al. (2014).

    Parameters
    ----------
    data : pandas dataframe
        Input experimental data.

    Returns
    -------
    regressor : object
        Linear regression parameters.
    """

    regressor = LinearRegression()
    regressor.fit(X_train, y_train)

    return(regressor)


def cdf_embedment_strength(data, N, angle,
                           wood_type='coniferous',
                           fastener_type='dowels'):
    """
    Compute and plot the cumulative distribution function of each
    simulated grouped simulated properties. Coniferous (softwood) and
    deciduous (hardwood) as well as nails and dowelled connections
    are considered. A 2x2 grid of 4 subplots is generated. One plot
    per group considered.

    Parameters
    ----------
    df : pandas dataframe
        Experimental data of data group.
    N : int
        Number of samples to simulate.
    wood_type : str, optional
        Type of wood in data group. The default is 'coniferous'.
    fastener_type : str, optional
        Type of fastener in data group. The default is 'dowel'.

    Returns
    -------
    fh : numpy ndarray
        Computed embedment strength.
    """
 
    X = np.log(data[['Density (kg/m3)', 'Diameter (mm)']])
    y = np.log(data['Embedment Strength (MPa)'])
    n = 100
    R2 = np.zeros(n)
    R2_stats = np.zeros(2)
    # scores = np.zeros((n, 4))
    
    for i in range(n):
        X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                            train_size=0.8)
        reg_leijten = leijten_model(X_train, y_train)
        y_pred = reg_leijten.predict(X_test)
        R2[i] = explained_variance_score(y_test, y_pred)
    
        y0 = data['Embedment Strength (MPa)'].to_numpy()
        y0 = np.sort(y0)
        x0 = np.linspace(0, 1, y0.shape[0])
        fh_exp = np.column_stack((x0, y0))

        # simulate properties
        rho, d = simulate_properties(N, data)
    
        # compute fh
        if fastener_type == 'nails':
            fh = embedment_strength_nails(rho, d, angle, reg_leijten)
        elif fastener_type == 'dowels':
            fh = embedment_strength_dowels(rho, d, angle, reg_leijten)
        elif fastener_type == 'lag screws':
            fh = embedment_strength_dowels(rho, d, angle, reg_leijten)
        else:
            print('Not supported fastener type!')
            
    R2_stats[0] = np.mean(R2)
    R2_stats[1] = np.std(R2)

    return(fh_exp, fh, R2_stats)
